<?php

namespace Tests\Fixtures\Annotation;

use Compass\ServiceHandler\Annotation\Service;

/**
 * @Service(id="sample_bundle.sample_class")
 * @package Compass\ServiceHandler\Tests\Annotation
 */
class SimpleClass
{
}

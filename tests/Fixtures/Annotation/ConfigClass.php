<?php

namespace Tests\Fixtures\Annotation;

use Compass\ServiceHandler\Annotation\Service;

/**
 * @Service(id="sample_bundle.copy_sample_class", class="Compass\ServiceHandler\Tests\Fixtures\Annotation\SimpleClass")
 * @package Compass\ServiceHandler\Tests\Annotation
 */
class ConfigClass
{
}

<?php

namespace Tests\Fixtures\Annotation;

use Compass\ServiceHandler\Annotation\Service;

/**
 * @Service(id=1)
 * @package Compass\ServiceHandler\Tests\Annotation
 */
class WrongClass
{
}

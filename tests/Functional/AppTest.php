<?php

namespace Compass\ServiceHandler\tests\Functional;

use Compass\ServiceHandler\Processor;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

class AppTest extends TestCase
{
    use ProphecyTrait;

    private $io;

    /**
     * @var Processor
     */
    private $processor;

    protected function setUp(): void
    {
        parent::setUp();

        $this->io = $this->prophesize('Composer\IO\IOInterface');
        $this->processor = new Processor($this->io->reveal());
    }

    /**
     * @dataProvider provideConfiguration
     */
    public function testApp(array $config)
    {
        chdir(__DIR__);

        $actual = $this->processor->processFile($config);

        $expected = [
            'App\\' => [
                'services' => [
                    'App\Service\BarService' => [
                        'public' => true,
                        'autowire' => true,
                        'autoconfigure' => false,
                    ],
                    'test.baz_service' => [
                        'class' => 'App\Service\BazService',
                        'public' => true,
                        'autowire' => true,
                        'autoconfigure' => false,
                    ],
                    'App\Service\Mailer\FooMailManager' => [
                        'public' => true,
                        'parent' => 'App\Mailer\MailManager',
                        'autowire' => true,
                        'autoconfigure' => false,
                    ],
                    'App\Service\FooService' => [
                        'public' => true,
                        'autowire' => true,
                        'autoconfigure' => false,
                    ],
                    'App\EventListener\FooControllerListener' => [
                        'arguments' => [
                            '@service_container',
                        ],
                        'tags' => [
                            [
                                'name' => 'kernel.event_listener',
                                'event' => 'kernel.controller',
                                'method' => 'onKernelController',
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $this->assertSame($expected, $actual);
    }

    public static function provideConfiguration()
    {
        return [
            'config' => [
                [
                    'file' => '../Functional/config/services.yaml',
                ],
            ],
        ];
    }
}
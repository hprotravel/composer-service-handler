<?php

namespace App\Service;

use Compass\ServiceHandler\Annotation\Service;

/**
 * @Service(
 *     id="App\Service\FooService",
 *     public=true,
 *     autowire=true,
 *     autoconfigure=false
 * )
 */
class FooService
{

}

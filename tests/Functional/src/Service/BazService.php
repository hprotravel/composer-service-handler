<?php

namespace App\Service;

use Compass\ServiceHandler\Annotation\Service;

/**
 * @Service(
 *     id="test.baz_service",
 *     public=true,
 *     autowire=true,
 *     autoconfigure=false
 * )
 */
class BazService
{

}

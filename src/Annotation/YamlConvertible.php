<?php

namespace Compass\ServiceHandler\Annotation;

interface YamlConvertible
{
    public function toYamlArray(): array;
}
